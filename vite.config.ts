import { defineConfig } from "vite";
import preact from "@preact/preset-vite";
import markdown, { Mode } from "vite-plugin-markdown";
import image from "@rollup/plugin-image";
import react from "@vitejs/plugin-react-refresh";
import envCompatible from "vite-plugin-env-compatible";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        // preact(),
        envCompatible({
            prefix: "VITE",
        }),
        react(),
        markdown({
            mode: [Mode.TOC, Mode.HTML, Mode.REACT],
        }),
        image(),
    ],
    /*     resolve: {
        alias: {
            react: "preact/compat",
            "react-dom": "preact/compat",
        },
    }, */
    publicDir: "src/public",
});

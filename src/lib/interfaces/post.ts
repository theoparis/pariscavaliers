export interface BlogPost {
    title: string;
    updatedAt: Date;
    content: string;
}

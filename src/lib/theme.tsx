import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme({
    colors: {},
    config: {
        useSystemColorMode: false,
        initialColorMode: "dark",
    },
});

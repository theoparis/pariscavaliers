import React, { useEffect, useState } from "react";
import ky from "ky";
import markdown, { MarkdownConfigType } from "markdown-pro";
import { BlogPost } from "../lib/interfaces/post";
import { parse } from "fecha";
import { Text, Heading } from "@chakra-ui/react";

const apiUrl = process.env.VITE_API_URL;
const markdownConfig: MarkdownConfigType = {
    useWrapper: false,
};

export const BlogPage = () => {
    const [posts, setPosts] = useState<BlogPost[]>([]);

    useEffect(() => {
        ky(`${apiUrl}/posts/find?APIKEY=${process.env.VITE_API_KEY}`)
            .json()
            .then(
                (res) =>
                    ((res as BlogPost[]).map((post) => ({
                        ...post,
                        updatedAt: parse(
                            (post.updatedAt as unknown) as string,
                            "M/D/YYYY"
                        ),
                    })) as unknown) as BlogPost[]
            )
            .then((res) => setPosts(res));
    }, []);

    return (
        <>
            {posts.map((post) => (
                <>
                    <Heading as="h2">{post.title}</Heading>
                    <Text fontSize="0.em">
                        Updated: {post.updatedAt.toLocaleDateString()}
                    </Text>
                    <Text
                        dangerouslySetInnerHTML={{
                            __html: markdown(post.content, markdownConfig),
                        }}
                    ></Text>
                </>
            ))}
        </>
    );
};

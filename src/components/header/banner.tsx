import React from "react";
import banner from "../../content/images/puppies1.jpg";
import { PropsWithChildren } from "react";
import { Image } from "@chakra-ui/react";

export const Banner = (props: PropsWithChildren<Record<string, unknown>>) => (
    <Image width="100%" src={banner} {...props} />
);
